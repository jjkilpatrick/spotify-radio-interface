var config = require('./config');
var express = require('express');
var request = require('request');
var async = require('async');
var Q = require('q');
var util = require('util');
var querystring = require('querystring');
var exphbs = require('express3-handlebars');
var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');
var app = express();

app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

app.get('/', function(req, res) {
    res.render('home');
});

app.get('/login', function(req, res) {
    console.log(config.spotify.scope);
    res.redirect('https://accounts.spotify.com/authorize?' +
        querystring.stringify({
            response_type: 'code',
            client_id: config.spotify.clientid,
            scope: config.spotify.scope,
            redirect_uri: config.spotify.redirectUri,
            state: config.spotify.state
        }));
});

app.get('/callback', function(req, res) {

    var code = req.query.code || null;
    var state = req.query.state || null;
    var storedState = req.cookies ? req.cookies[stateKey] : null;

    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        form: {
            code: code,
            redirect_uri: config.spotify.redirectUri,
            grant_type: 'authorization_code',
            client_id: config.spotify.clientid,
            client_secret: config.spotify.clientSecret
        },
        json: true
    };

    request.post(authOptions, function(error, response, body) {
        if (!error && response.statusCode === 200) {

            localStorage.setItem('access_token', body.access_token);
            localStorage.setItem('refresh_token', body.refresh_token);

            res.redirect('/user');

        } else {
            res.redirect('/#' +
                querystring.stringify({
                    error: 'invalid_token'
                }));
        }
    });

});

app.get('/user', function(req, res) {

    getUser().then(function(user) {
        getPlaylists(user.id).then(function(playlists) {
            getCompletePlaylists(user.id, playlists);

            res.render('user', {
                country: user.country,
                display_name: user.display_name,
                email: user.email,
                href: user.href,
                id: user.id,
                image: user.images[0].url
            });

        });
    });

});

refreshToken = function() {
    console.log('Refresh token: ' + localStorage.getItem('refresh_token'));
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        headers: {
            'Authorization': 'Basic ' + (new Buffer(config.spotify.clientid + ':' + config.spotify.clientSecret).toString('base64'))
        },
        form: {
            grant_type: 'refresh_token',
            refresh_token: localStorage.getItem('refresh_token')
        },
        json: true
    };

    request.post(authOptions, function(error, response, body) {
        if (!error && response.statusCode === 200) {
            localStorage.setItem('access_token', body.access_token);
        }
    });
}

getUser = function() {

    var deferred = Q.defer();

    var user = {
        url: 'https://api.spotify.com/v1/me',
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('access_token')
        },
        json: true
    };

    request.get(user, function(error, response, body) {
        if (!error && response.statusCode === 200) {
            deferred.resolve(body);
        } else {
            refreshToken();
        }
    });

    return deferred.promise;

}

getPlaylists = function(id) {

    var deferred = Q.defer(),
        playlistArr = [];

    var playlists = {
        url: 'https://api.spotify.com/v1/users/' + id + '/playlists',
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('access_token')
        },
        json: true
    };

    request.get(playlists, function(error, response, body) {
        if (!error && response.statusCode === 200) {

            for (var i = body.items.length - 1; i >= 0; i--) {
                playlistArr.push(
                    body.items[i].id
                )
            };

            deferred.resolve(playlistArr);

        } else {
            refreshToken();
        }
    });

    return deferred.promise;

}

getCompletePlaylists = function(id, playlists) {

    var completePlaylistArr = [];

    // Last item seems to be null so pop it off
    // playlists.pop();
    // 
    // var playlists = [
    //     '7amCw4szxQIFYimelyZSnJ',
    //     '0A87cwI0bEJtJwDfXZQOab'
    // ];

    async.each(playlists, function(playlist, callback) {

        console.log(playlist);

        var target = {
            url: 'https://api.spotify.com/v1/users/' + id + '/playlists/' + playlist,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('access_token')
            },
            json: true
        }

        request.get(target, function(error, response, body) {
            console.log(body);
            // if (!error && response.statusCode === 200) {

            //     var name = body.name;
            //     var id = body.id;

            //     try {
            //         var image = body.tracks.items[0].track.album.images[0].url;
            //     } catch (e) {
            //         var image = null;
            //     }

            //     if (image.length > 0) {
            //         completePlaylistArr.push({
            //             'name': name,
            //             'id': id,
            //             'image': image
            //         });
            //         callback();
            //     };

            // }
        });

    }, function(err) {
        if (err) {
            console.log('Many fail');
        } else {
            console.log('Much completed');
            console.log(completePlaylistArr);
        }
    });


    // var deferred = Q.defer();
    // completePlaylistArr = [];

    // // deferred.resolve(playlists);

    // async.each(playlists, function(playlist, callback) {

    //         console.log(playlists);

    //         // var target = {
    //         //     url: 'https://api.spotify.com/v1/users/' + id + '/playlists/' + playlist,
    //         //     headers: {
    //         //         'Authorization': 'Bearer ' + localStorage.getItem('access_token')
    //         //     },
    //         //     json: true
    //         // }

    //         // request.get(target, function(error, response, body) {
    //         //     if (!error && response.statusCode === 200) {

    //         //         var name = body.name;
    //         //         var id = body.id;

    //         //         try {
    //         //             var image = body.tracks.items[0].track.album.images[0].url;
    //         //         } catch (e) {
    //         //             var image = null;
    //         //         }

    //         //         completePlaylistArr.push({
    //         //             'name': name,
    //         //             'id': id,
    //         //             'image': image
    //         //         });

    //         //         callback();
    //         //     }
    //         // });
    //     },
    //     function(err) {
    //         deferred.resolve(completePlaylistArr);
    //     });

    // return deferred.promise;

}

app.listen(8888);